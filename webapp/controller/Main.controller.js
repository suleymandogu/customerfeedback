sap.ui.define([
	"com/sdogu/Routing_Template/controller/BaseController",
	'sap/ui/model/json/JSONModel',
	"jquery.sap.global"
], function (BaseController, JSONModel, jquery) {
	"use strict";

	return BaseController.extend("com.sdogu.Routing_Template.controller.Main", {

		onPrint: function (oEvent) {
			var oTarget = this.getView(),
				sTargetId = oEvent.getSource().data("targetId");

			if (sTargetId) {
				oTarget = oTarget.byId(sTargetId);
			}

			if (oTarget) {
				var $domTarget = oTarget.$()[0],
					sTargetContent = $domTarget.innerHTML,
					sOriginalContent = document.body.innerHTML;

				document.body.innerHTML = sTargetContent;
				window.print();
				document.body.innerHTML = sOriginalContent;
			} else {
				jQuery.sap.log.error("onPrint needs a valid target container [view|data:targetId=\"SID\"]");
			}
		},

		onInit: function () {
			var that = this,
				oComp = that.getOwnerComponent(),
				oView = that.getView(),
				jsonModel = oComp.getModel("myJSON");

			var oObjectHeader = this.getView().byId("oh1");

			var data = [{
				title: "Scan Barcode"
			}, {
				title: ""
			}];

			var data2 = [{
				"icon": "sap-icon://message-warning",
				"number": "4",
				"title": "Sample Orders",
				"infoState": "Error",
				"info": "UNRATED",
				"visible": true
			}, {
				"icon": "sap-icon://message-success",
				"number": "30",
				"title": "Sample Orders",
				"infoState": "Success",
				"info": "RATED",
				"visible": true
			}, {
				"icon": "sap-icon://message-success",
				"number": "30",
				"title": "Sample Orders",
				"infoState": "Success",
				"info": "RATED",
				"visible": false
			}, {
				"icon": "sap-icon://message-success",
				"number": "30",
				"title": "Sample Orders",
				"infoState": "Success",
				"info": "RATED",
				"visible": false
			}];
			oView.addStyleClass(oComp.getContentDensityClass());
			jsonModel.setProperty("/selectionList", data);
			jsonModel.setProperty("/TileCollection", data2);
			oObjectHeader.setTitle(" ");
			oView.byId("scanBtnId").setVisible(true);

			jsonModel.setProperty("/thisMonth", new Date().toLocaleString('en-GB', {
				timeZone: 'UTC'
			}));
		},

		onScan: function (oEvent) {
			var that = this,
				oComp = that.getOwnerComponent(),
				jsonModel = oComp.getModel("myJSON");
			jsonModel.setProperty("/isMain", true);
			this.getRouter().navTo("BeverageTasting");
		},

		onGo: function (oEvent) {
			var that = this,
				oComp = that.getOwnerComponent(),
				jsonModel = oComp.getModel("myJSON");
			jsonModel.setProperty("/isMain", true);
			this.getRouter().navTo("BeverageTasting");
		},

		onPressSlideTile: function (oEvent) {
			var that = this,
				id = oEvent.getParameter("id").split("--")[1],
				oComp = that.getOwnerComponent(),
				dizi = [],
				jsonModel = oComp.getModel("myJSON");

			if (id === "slideTile1Id" || id === "slideTile2Id") {
				jsonModel.setProperty("/rated", false);
				jsonModel.setProperty("/highlight", "Error");
				dizi.push({
					CustomerId: "100000",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100001",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100002",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100003",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100004",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100005",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100006",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100007",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100007",
					CustomerName: "Sales Order Number"
				});
				jsonModel.setProperty("/OrderList", dizi);
			} else if (id === "slideTile3Id" || id === "slideTile4Id") {
				jsonModel.setProperty("/rated", true);
				jsonModel.setProperty("/highlight", "Success");
				dizi.push({
					CustomerId: "100021",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100022",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100023",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100024",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100025",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100026",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100027",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100028",
					CustomerName: "Sales Order Number"
				});
				dizi.push({
					CustomerId: "100029",
					CustomerName: "Sales Order Number"
				});
				jsonModel.setProperty("/OrderList", dizi);
			}
			this.getRouter().navTo("Master");
		},

		handleItemSelect: function (oEvent) {
			var oItem = oEvent.getParameter("listItem");
			var oObjectHeader = this.getView().byId("oh1");
			oObjectHeader.setTitle(oItem.getTitle());
			oObjectHeader.setBindingContext(oItem.getBindingContext());
			this._oPopover.close();
			this.getView().byId("scanBtnId").setVisible(oItem.getTitle() === "Scan Barcode");
		},

		handleTitleSelectorPress: function (oEvent) {
			var that = this,
				_oPopover = that._getResponsivePopover(),
				oComp = that.getOwnerComponent();
			_oPopover.setModel(oComp.getModel("myJSON"));
			_oPopover.openBy(oEvent.getParameter("domRef"));
		},

		_getResponsivePopover: function () {
			if (!this._oPopover) {
				this._oPopover = sap.ui.xmlfragment("com.sdogu.Routing_Template.fragments.title", this);
			}
			return this._oPopover;
		},

		onPress: function (oEvent) {
			//	var oControl = this.getControl("goToDetail1Id");
			var id = oEvent.getParameter("id").split("--")[1];
			switch (id) {
			case "goToDetail1Id":
				this.getRouter().navTo("DetailHeader");
				break;
			case "goToMasterId":
				this.getRouter().navTo("Master");
				break;
			default:
			}
		}
	});

});