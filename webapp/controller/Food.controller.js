sap.ui.define([
	"com/sdogu/Routing_Template/controller/BaseController",
], function (BaseController) {
	"use strict";

	return BaseController.extend("com.sdogu.Routing_Template.controller.Food", {

		onInit: function () {
			var that = this;
			that.getView().addStyleClass(that.getOwnerComponent().getContentDensityClass());
		},

		onBackDetail: function () {
			var that = this,
				oComp = that.getOwnerComponent(),
				jsonModel = oComp.getModel("myJSON");

			var isMain = jsonModel.getProperty("/isMain");
			var dizi = jsonModel.getProperty("/OrderList");

			if (isMain) {
				that.getRouter().navTo("Main");
				return;
			}

			that.getRouter().navTo("Detail", {
				id: dizi[jsonModel.getProperty("/index")].CustomerId
			});
		},

		_onButtonPress1: function () {
				var that = this,
					oComp = that.getOwnerComponent(),
					jsonModel = oComp.getModel("myJSON");
				var dizi = jsonModel.getProperty("/OrderList");

				that.getRouter().navTo("Overall");
			}
			/**
			 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
			 * @memberOf com.sdogu.Routing_Template.view.Food
			 */
			//	onExit: function() {
			//
			//	}

	});

});