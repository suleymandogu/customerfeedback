sap.ui.define([
	"com/sdogu/Routing_Template/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("com.sdogu.Routing_Template.controller.Thanks", {

		_onButtonPress: function (oEvent) {
			var that = this;
			that.getRouter().navTo("Main");

			that.getView().addStyleClass(that.getOwnerComponent().getContentDensityClass());
		},

		onPressSlideTile: function (oEvent) {
			var that = this,
				id = oEvent.getParameter("id").split("--")[1],
				oComp = that.getOwnerComponent(),
				dizi = [],
				jsonModel = oComp.getModel("myJSON");

			if (id === "slideTile3Id" || id === "slideTile4Id") {
				jsonModel.setProperty("/rated", true);
				jsonModel.setProperty("/highlight", "Success");
				dizi.push({
					CustomerId: "100021",
					CustomerName: "Order Number"
				});
				dizi.push({
					CustomerId: "100022",
					CustomerName: "Order Number"
				});
				dizi.push({
					CustomerId: "100023",
					CustomerName: "Order Number"
				});
				dizi.push({
					CustomerId: "100024",
					CustomerName: "Order Number"
				});
				dizi.push({
					CustomerId: "100025",
					CustomerName: "Order Number"
				});
				dizi.push({
					CustomerId: "100026",
					CustomerName: "Order Number"
				});
				dizi.push({
					CustomerId: "100027",
					CustomerName: "Order Number"
				});
				dizi.push({
					CustomerId: "100028",
					CustomerName: "Order Number"
				});
				dizi.push({
					CustomerId: "100029",
					CustomerName: "Order Number"
				});
				jsonModel.setProperty("/OrderList", dizi);
			}
			this.getRouter().navTo("Master");
		},
	});

});