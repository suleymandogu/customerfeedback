sap.ui.define([
	"com/sdogu/Routing_Template/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("com.sdogu.Routing_Template.controller.Master", {

		onInit: function () {
			var that = this,
				oComp = that.getOwnerComponent(),
				oView = that.getView(),
				jsonModel = oComp.getModel("myJSON");
			oView.addStyleClass(oComp.getContentDensityClass());

			that.getRouter("Master").attachRouteMatched(that._onRouteMatched, that);
		},

		_onRouteMatched: function (oEvent) {
			var that = this,
				oComp = that.getOwnerComponent(),
				jsonModel = oComp.getModel("myJSON"),
				rated = jsonModel.getProperty("/rated"),
				sRouteName = oEvent.getParameter("name");

			if (sRouteName !== "Master") {
				return;
			}

			var dizi = jsonModel.getProperty("/OrderList");

			if (!rated) {
				that.getView().byId("pageMasterId").setTitle("Unrated Sample Orders");
			} else {
				that.getView().byId("pageMasterId").setTitle("Rated Sample Orders");
			}

			jsonModel.setProperty("/index", 0);

			// that.getRouter().navTo("Detail", {
			// 	id: dizi[0].CustomerId
			// });
		},

		backtoMain: function (oEvent) {
			var that = this;
			that.getRouter().navTo("Main");
		},

		onItemPressListing: function (oEvent) {
			var that = this,
				OrderNo = "",
				jsonModel = that.getOwnerComponent().getModel("myJSON"),
				sId = oEvent.getParameter("listItem").getBindingContext("myJSON").sPath.split("/")[2];
			var dizi = jsonModel.getProperty("/OrderList");
			jsonModel.setProperty("/index", sId);
			that.getRouter().navTo("Detail", {
				id: dizi[sId].CustomerId
			});
		},

		onPress: function (oEvent) {
			//	var oControl = this.getControl("goToDetail1Id");
			var id = oEvent.getParameter("id").split("--")[1];
			switch (id) {
			case "goToDetail1Id":
				this.getRouter().navTo("Detail", {
					id: 22
				});
				break;
			case "goToMasterId":
				this.getRouter().navTo("Master");
				break;
			default:
			}
		}
	});

});