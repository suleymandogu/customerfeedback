sap.ui.define([
	"com/sdogu/Routing_Template/controller/BaseController"
], function (BaseController) {
	"use strict";

	return BaseController.extend("com.sdogu.Routing_Template.controller.Detail", {
		onInit: function () {
			var that = this,
				oComp = that.getOwnerComponent(),
				oView = that.getView(),
				jsonModel = oComp.getModel("myJSON");
			oView.addStyleClass(oComp.getContentDensityClass());

			that.getRouter("Detail").attachRouteMatched(that._onRouteMatched, that);
		},

		backtoMain: function (oEvent) {
			var that = this;
			that.getRouter().navTo("Master");
		},

		onPressTableDetail: function (oEvent) {
			var that = this,
				oComp = that.getOwnerComponent(),
				jsonModel = oComp.getModel("myJSON");
			var listItem = oEvent.getParameter("listItem");
			var prop = null;
			var index = null;
			var orderNumber = null
			if (typeof listItem === "undefined") {
				prop = oEvent.getSource().getProperty("type");
				index = oEvent.getSource().getBindingContext("myJSON").sPath.split("/")[2];
				orderNumber = jsonModel.getProperty(oEvent.getSource().getBindingContext("myJSON").sPath);
			} else {
				prop = oEvent.getParameter("listItem").getProperty("type");
				index = oEvent.getParameter("listItem").getBindingContext("myJSON").sPath.split("/")[2];
				orderNumber = jsonModel.getProperty(oEvent.getParameter("listItem").getBindingContext("myJSON").sPath);
			}

			jsonModel.setProperty("/isMain", false);
			if (prop === "Navigation") {
				if (parseInt(index) % 2 === 0) {
					that.getRouter().navTo("FoodDisplay");
				} else {
					that.getRouter().navTo("BeveageTastingDisplay");
				}

			} else {
				if (parseInt(index) % 2 === 0) {
					that.getRouter().navTo("Food");
				} else {
					that.getRouter().navTo("BeverageTasting");
				}
			}
		},

		_onRouteMatched: function (oEvent) {
			var that = this,
				oComp = that.getOwnerComponent(),
				jsonModel = oComp.getModel("myJSON"),
				rated = jsonModel.getProperty("/rated"),
				number = oEvent.getParameter("arguments").id,
				sRouteName = oEvent.getParameter("name");

			var det = [];

			if (sRouteName !== "Detail") {
				return;
			}

			var index = jsonModel.getProperty("/index");

			var dizi = jsonModel.getProperty("/OrderList");
			jsonModel.setProperty("/CustomerName", "Doehler");
			jsonModel.setProperty("/CustomerId", number);

			index = parseInt(index);

			for (var i = 0; i < index + 4; i++) {
				det.push({
					ItemNumber: (i + 1) * 10,
					Equnr: "Material " + (i + 1) * 10,
					Category: "Alk. Mixdrink Ingwer-Whisky Alk. 9,5% Vo " + (i + 1) * 10,
					text: i % 2 === 0 ? "Food" : "Beverage",
					colorScheme: i % 2 === 0 ? 6 : 7
				});
			}

			jsonModel.setProperty("/MainListDetail", det);

		},
	});

});