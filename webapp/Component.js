sap.ui.define([
	"sap/ui/core/UIComponent",
	"sap/ui/Device",
	"com/sdogu/Routing_Template/model/models",
	'sap/ui/model/json/JSONModel'
], function (UIComponent, Device, models, JSONModel) {
	"use strict";

	return UIComponent.extend("com.sdogu.Routing_Template.Component", {

		metadata: {
			manifest: "json"
		},

		/**
		 * The component is initialized by UI5 automatically during the startup of the app and calls the init method once.
		 * @public
		 * @override
		 */
		init: function () {
			// call the base component's init function
			UIComponent.prototype.init.apply(this, arguments);

			// enable routing
			this.getRouter().initialize();
			this.setModel(models.createJSONModel(), "myJSON");

			// set the device model
			this.setModel(models.createDeviceModel(), "device");

		},

		getContentDensityClass: function () {
			if (!this._sContentDensityClass) {
				if (!sap.ui.Device.support.touch) {
					this._sContentDensityClass = "sapUiSizeCompact";
				} else {
					this._sContentDensityClass = "sapUiSizeCozy";
				}
			}
			return this._sContentDensityClass;
		}
	});
});